package main

//go:generate go-bindata -o server/bindata.go  -pkg server  public/...

import "gitlab.com/andrewn/pebbledash/cmd"

func main() {
	cmd.Execute()
}
