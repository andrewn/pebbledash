package sources

import (
	"fmt"

	"gitlab.com/andrewn/pebbledash/model"

	"github.com/spf13/viper"
)

func newSource(config map[string]string) (model.Source, error) {
	sourceType := config["type"]

	switch sourceType {
	case "prometheus":
		return NewPrometheusSource(config)
	// Add new sources here as we add them
	default:
		return nil, fmt.Errorf("Invalid sourceType %v", sourceType)
	}
}

// ConfigureSources returns a Sources object, configured from the
// viper configuration
func ConfigureSources() (map[string]model.Source, error) {
	sources := make(map[string]model.Source)

	sourceConfig := viper.GetStringMap("sources")

	for k := range sourceConfig {
		valueMap := viper.GetStringMapString("sources." + k)

		source, err := newSource(valueMap)
		if err != nil {
			return nil, err
		}

		sources[k] = source
	}

	return sources, nil
}
