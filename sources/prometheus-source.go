package sources

import (
	"context"
	"time"

	"fmt"

	"github.com/prometheus/client_golang/api/prometheus"
	pModel "github.com/prometheus/common/model"
	"gitlab.com/andrewn/pebbledash/model"
)

type prometheusSource struct {
	queryAPI prometheus.QueryAPI
}

func (t prometheusSource) ExecQuery(ctx context.Context, query string, start time.Time, end time.Time, resolution time.Duration) ([]model.Point, error) {

	r := prometheus.Range{
		Start: start,
		End:   end,
		Step:  resolution,
	}

	value, err := t.queryAPI.QueryRange(ctx, query, r)

	if err != nil {
		return nil, err
	}

	m, ok := value.(pModel.Matrix)
	if !ok {
		return nil, fmt.Errorf("Expected a matrix")
	}

	if m.Len() < 1 {
		return nil, fmt.Errorf("Expected results")
	}

	stream := m[0]

	results := make([]model.Point, len(stream.Values))

	for i, v := range stream.Values {
		results[i] = model.Point{
			Time:  time.Unix(int64(v.Timestamp/1000), 0),
			Value: float64(v.Value),
		}
	}

	return results, nil
}

// NewPrometheusSource creates a prometheus-based Source implementation
func NewPrometheusSource(config map[string]string) (model.Source, error) {
	address := config["address"]

	cfg := prometheus.Config{
		Address: address,
	}

	client, err := prometheus.New(cfg)
	if err != nil {
		return nil, err
	}

	queryAPI := prometheus.NewQueryAPI(client)

	return prometheusSource{queryAPI}, nil
}
