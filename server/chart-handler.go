package server

import (
	"net/http"

	"time"

	"fmt"

	"github.com/gorilla/mux"
	"github.com/wcharczuk/go-chart"
)

func checkNotModified(res http.ResponseWriter, req *http.Request, modtime time.Time) bool {
	t, err := time.Parse(http.TimeFormat, req.Header.Get("If-Modified-Since"))

	// The Date-Modified header truncates sub-second precision, so
	// use mtime < t+1s instead of mtime <= t to check for unmodified.
	if err == nil && modtime.Before(t.Add(1*time.Second)) {
		h := res.Header()
		delete(h, "Content-Type")
		delete(h, "Content-Length")
		res.WriteHeader(http.StatusNotModified)
		return true
	}

	res.Header().Set("Last-Modified", modtime.UTC().Format(http.TimeFormat))
	return false
}

// getTimeParametersFromDuration will return a tuple of
// start time, end time, time resolution and max cache age
func getTimeParametersFromDuration(durationString string) (time.Time, time.Time, time.Duration, int32) {
	duration := asDuration(durationString)
	resolution := getResolutionForDuration(duration)

	now := time.Now()
	end := now.Truncate(resolution)

	// These results can be cached up until right before the next
	// time window starts
	maxCacheAge := int32(end.Add(resolution).Sub(now).Seconds()) - 1
	if maxCacheAge < 0 {
		maxCacheAge = 0
	}

	start := end.Add(time.Duration(-duration.Nanoseconds()))

	return start, end, resolution, maxCacheAge
}

func (c *appContext) ChartHandler(res http.ResponseWriter, req *http.Request) (int, error) {
	vars := mux.Vars(req)
	key := vars["key"]

	// Work out what we'll be graphing
	start, end, resolution, maxCacheAge := getTimeParametersFromDuration(vars["duration"])

	// If we're still in the same time period and the client
	// already has this data, no need to proceed
	if checkNotModified(res, req, end) {
		return http.StatusNotModified, nil
	}

	if maxCacheAge > 0 {
		res.Header().Set("Cache-Control", fmt.Sprintf("max-age=%d", maxCacheAge))
	}

	chartDefinition, ok := c.charts[key]
	if !ok {
		return http.StatusNotFound, nil
	}

	valueFormatter := chartDefinition.YAxisValueFormatter

	source := chartDefinition.Source
	if source == nil {
		return http.StatusNotFound, nil
	}

	ts, err := source.ExecQuery(req.Context(), chartDefinition.Query, start, end, resolution)

	if err != nil {
		return http.StatusInternalServerError, err
	}

	xValues := make([]float64, len(ts))
	yValues := make([]float64, len(ts))

	for i, v := range ts {
		xValues[i] = float64(v.Time.Unix())
		yValues[i] = v.Value
	}

	graph := chart.Chart{
		Height: 400,
		Width:  800,
		YAxis: chart.YAxis{
			ValueFormatter: valueFormatter,
			Style: chart.Style{
				Show: true,
			},
		},
		XAxis: chart.XAxis{
			Range: &chart.ContinuousRange{
				Min: float64(start.Unix()),
				Max: float64(end.Unix()),
			},
		},
		Series: []chart.Series{
			chart.ContinuousSeries{
				XValues: xValues,
				YValues: yValues,
			},
		},
	}

	res.Header().Set("Content-Type", "image/png")
	graph.Render(chart.PNG, res)

	return http.StatusOK, nil
}
