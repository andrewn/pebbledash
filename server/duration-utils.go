package server

import "time"

func asDuration(durationString string) time.Duration {
	switch durationString {
	case "day":
		return 24 * time.Hour
	case "week":
		return 7 * 24 * time.Hour
	default:
		return 31 * 24 * time.Hour
	}
}

func getResolutionForDuration(duration time.Duration) time.Duration {
	switch {
	case duration <= 24*time.Hour:
		return 1 * time.Minute

	case duration <= 7*24*time.Hour:
		return 10 * time.Minute

	default:
		return time.Hour
	}
}
