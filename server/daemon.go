package server

import (
	"log"
	"net/http"

	assetfs "github.com/elazarl/go-bindata-assetfs"
	"github.com/gorilla/mux"

	"github.com/spf13/viper"
)

// SafeHandler is like a http.HandlerFunc but safe
type SafeHandler func(w http.ResponseWriter, r *http.Request) (int, error)

func adapt(fn SafeHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		status, err := fn(w, r)
		if err != nil {
			log.Printf("HTTP %d: %q", status, err)
			switch status {
			case http.StatusNotFound:
				http.NotFound(w, r)
				// And if we wanted a friendlier error page:
				// err := ah.renderTemplate(w, "http_404.tmpl", nil)
			case http.StatusInternalServerError:
				http.Error(w, http.StatusText(status), status)
			default:
				http.Error(w, http.StatusText(status), status)
			}
		}

	}
}

// StartDaemon starts the server process
func StartDaemon() error {
	context, err := newAppContext()
	if err != nil {
		return err
	}

	r := mux.NewRouter()

	r.HandleFunc("/charts/{duration:(?:day|week|month)}/{key}", adapt(context.ChartHandler))
	r.HandleFunc("/past/{duration:(?:day|week|month)}", adapt(context.PastHandler))
	r.HandleFunc("/", adapt(context.IndexHandler))

	r.Handle("/public/{p:(?:.*)}", http.FileServer(&assetfs.AssetFS{
		Asset:     Asset,
		AssetDir:  AssetDir,
		AssetInfo: AssetInfo,
		Prefix:    "",
	}))

	http.Handle("/", r)

	return http.ListenAndServe(viper.GetString("server.listen"), nil)
}
