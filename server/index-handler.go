package server

import (
	"net/http"

	"gitlab.com/andrewn/pebbledash/model"

	"github.com/gorilla/mux"
)

type indexValues struct {
	ChartMap map[string]model.ChartDefinition
	Duration string
}

func (c *appContext) IndexHandler(res http.ResponseWriter, req *http.Request) (int, error) {
	return c.renderIndex("day", res, req)
}

func (c *appContext) PastHandler(res http.ResponseWriter, req *http.Request) (int, error) {
	vars := mux.Vars(req)
	duration := vars["duration"]

	return c.renderIndex(duration, res, req)
}

func (c *appContext) renderIndex(duration string, res http.ResponseWriter, req *http.Request) (int, error) {
	res.Header().Set("Content-Type", "text/html")

	c.templates["index"].Execute(res, &indexValues{
		ChartMap: c.charts,
		Duration: duration,
	})

	return http.StatusOK, nil
}
