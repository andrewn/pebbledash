package server

import (
	"html/template"

	"gitlab.com/andrewn/pebbledash/model"
	"gitlab.com/andrewn/pebbledash/sources"
)

type appContext struct {
	charts    map[string]model.ChartDefinition
	templates map[string]*template.Template
}

func loadTemplates() (map[string]*template.Template, error) {
	indexBytes, err := Asset("public/index.html")
	if err != nil {
		return nil, err
	}

	s := string(indexBytes[:])
	t, err := template.New("index").Parse(s)
	if err != nil {
		return nil, err
	}

	result := make(map[string]*template.Template)
	result["index"] = t
	return result, nil
}

func newAppContext() (*appContext, error) {
	templates, err := loadTemplates()
	if err != nil {
		return nil, err
	}

	sources, err := sources.ConfigureSources()
	if err != nil {
		return nil, err
	}

	charts, err := model.ConfigureCharts(sources)
	if err != nil {
		return nil, err
	}

	context := appContext{
		charts:    charts,
		templates: templates,
	}

	return &context, nil
}
