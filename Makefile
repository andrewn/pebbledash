.PHONY: target

all: install

install: gomake-all

install-deps: 
	go get -u github.com/jteeuwen/go-bindata/...

clean:
	rm -rf bin
	rm -rf ./server/bindata.go

gomake-all:
	mkdir -p bin
	go generate
	go build -o ./bin/pebbledash
