package model

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
	chart "github.com/wcharczuk/go-chart"
)

// ChartDefinition defines a chart definition
type ChartDefinition struct {
	Title               string
	Source              Source
	Query               string
	YAxisValueFormatter chart.ValueFormatter
}

// ConfigureCharts creates chart definitions using the configuration
func ConfigureCharts(sources map[string]Source) (map[string]ChartDefinition, error) {
	result := make(map[string]ChartDefinition)

	config := viper.GetStringMap("charts")
	for key := range config {

		chartConfig := viper.GetStringMapString("charts." + key)
		sourceName := chartConfig["source"]
		source := sources[sourceName]

		if source == nil {
			return nil, fmt.Errorf("Source %v not found", sourceName)
		}

		chart := ChartDefinition{
			Title:               chartConfig["title"],
			Source:              source,
			Query:               chartConfig["query"],
			YAxisValueFormatter: getYAxisFormatter(chartConfig),
		}

		result[key] = chart
	}

	return result, nil
}

const (
	kb = 1024
	mb = 1024 * kb
	gb = 1024 * mb
	tb = 1024 * gb
)

func bytesFormatter(v interface{}) string {
	unit := ""
	var value float64

	switch t := v.(type) {
	case int:
		value = float64(t)
	case float32:
		value = float64(t)
	case float64:
		value = t
	default:
		// TODO: can do better?
		value = 0
	}
	bytes := value

	switch {
	case bytes >= tb:
		unit = "TB"
		value = value / tb
	case bytes >= gb:
		unit = "GB"
		value = value / gb
	case bytes >= mb:
		unit = "MB"
		value = value / mb
	case bytes >= kb:
		unit = "KB"
		value = value / kb
	case bytes >= 1:
		unit = "B"
	case bytes == 0:
		return "0B"
	}

	stringValue := fmt.Sprintf("%.1f", value)
	stringValue = strings.TrimSuffix(stringValue, ".0")
	return fmt.Sprintf("%s%s", stringValue, unit)
}

func defaultFormatter(v interface{}) string {
	return fmt.Sprintf("%v", v)
}

func getYAxisFormatter(chartConfig map[string]string) chart.ValueFormatter {
	yaxisType := chartConfig["yaxis"]

	switch yaxisType {
	case "bytes":
		return bytesFormatter
	default:
		return defaultFormatter
	}
}
