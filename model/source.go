package model

import (
	"context"
	"time"
)

// Point contains a single timeseries datapoint
type Point struct {
	Time  time.Time
	Value float64
}

// Source will run a query
type Source interface {
	ExecQuery(ctx context.Context, query string, start time.Time, end time.Time, resolution time.Duration) ([]Point, error)
}
